package com.example.zador_000.restofinder;

/**
 * Created by zador_000 on 21-May-17.
 */

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class GeoPositionChecker implements LocationListener {

    public static Location imHere;

    public void setUpLocationListener(Context context)
    {
        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);

        LocationListener locationListener = new GeoPositionChecker();

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                30000,
                100,
                locationListener);

         imHere = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }

        @Override
        public void onLocationChanged(Location loc) {
            imHere = loc;
        }
        @Override
        public void onProviderDisabled(String provider) {}
        @Override
        public void onProviderEnabled(String provider) {}
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

}
