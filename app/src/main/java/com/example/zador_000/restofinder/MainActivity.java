package com.example.zador_000.restofinder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.zador_000.restofinder.networkWrappers.PlaceNetworkWrapper;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Spinner placeSpinner, priceLevelSpinner, distanceSpinner;
    GeoPositionChecker geoChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        placeSpinner = (Spinner) findViewById(R.id.placesSpinnerMainActivity);

        ArrayAdapter<CharSequence> placeAdapter = ArrayAdapter.createFromResource(this,
                R.array.places_array, android.R.layout.simple_spinner_item);

        placeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        placeSpinner.setAdapter(placeAdapter);

        priceLevelSpinner = (Spinner) findViewById(R.id.priceLevelSpinnerMainActivity);

        ArrayAdapter<CharSequence> priceAdapter = ArrayAdapter.createFromResource(this,
                R.array.price_array, android.R.layout.simple_spinner_item);

        priceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        priceLevelSpinner.setAdapter(priceAdapter);

        distanceSpinner = (Spinner) findViewById(R.id.distanceLevelSpinnerMainActivity);

        ArrayAdapter<CharSequence> distanceAdapter = ArrayAdapter.createFromResource(this,
                R.array.distance_array, android.R.layout.simple_spinner_item);

        distanceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        distanceSpinner.setAdapter(distanceAdapter);

        Button searchButton = (Button) findViewById(R.id.searchButtonMainActivity);
        searchButton.setOnClickListener(this);

        }

    @Override
    protected void onResume() {
        super.onResume();
        geoChecker = new GeoPositionChecker();
        geoChecker.setUpLocationListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    public void onClick(View v) {
       final String distance = distanceSpinner.getSelectedItem().toString();
       final String placeType = placeSpinner.getSelectedItem().toString().toLowerCase();
        Intent searchResultActivityIntent = new Intent(MainActivity.this, SearchResultActivity.class);
        searchResultActivityIntent.putExtra(SearchResultActivity.KEY_DISTANCE, distance);
        searchResultActivityIntent.putExtra(SearchResultActivity.KEY_TYPE, placeType);
        startActivity(searchResultActivityIntent);

    }
}