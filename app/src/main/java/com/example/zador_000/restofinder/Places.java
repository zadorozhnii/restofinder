package com.example.zador_000.restofinder;

import android.graphics.Bitmap;

/**
 * Created by zador_000 on 02-Jun-17.
 */

public class Places {
    private double latitude;
    private double longtidude;
    private String placeName;



    private Bitmap placeIcon;

    public Places (double latitude,double longtidude,String placeName, Bitmap placeIcon){
        this.latitude = latitude;
        this.longtidude = longtidude;
        this.placeName = placeName;
        this.placeIcon = placeIcon;
    }

    public double getLatitude() {
        return latitude;
    }
    public double getLongtidude()
    {
        return longtidude;
    }
    public String getPlaceName() {
        return placeName;
    }
    public Bitmap getPlaceIcon() {
        return placeIcon;
    }

}
