package com.example.zador_000.restofinder;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;

import com.example.zador_000.restofinder.networkWrappers.PlaceNetworkWrapper;
import com.google.android.gms.location.places.*;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class SearchResultActivity extends AppCompatActivity implements OnMapReadyCallback {

    public final static String KEY_DISTANCE = "KEY_DISTANCE";
    public final static String KEY_TYPE = "KEY_TYPE";
    String distance = "";
    String placeType = "";

    private GoogleMap mMap;
    Location imHere = GeoPositionChecker.imHere;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapFragmentSecondActivity);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if(bundle!=null){
                distance = bundle.getString(KEY_DISTANCE, "--EMPTY--");
                placeType = bundle.getString(KEY_TYPE, "--EMPTY--");
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng currentPosition = new LatLng(imHere.getLatitude(), imHere.getLongitude());
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(currentPosition)
                .zoom(15)
                .bearing(0)
                .tilt(10)
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        MarkerOptions marker = new MarkerOptions().position(currentPosition).title("I am here");
        marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_place));
        mMap.addMarker(marker);
         new Thread(new Runnable() {
             @Override
             public void run() {
               final  ArrayList<Places> arrayListPlaces = new PlaceNetworkWrapper(Integer.parseInt(distance),placeType).placesSearch();
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     putMarkers(arrayListPlaces);
                 }
             });
             }
         }).start();


    }

    public void putMarkers (ArrayList<Places> arrayList){
        for (int i = 0; i<arrayList.size(); i++){
            LatLng currentPosition = new LatLng(arrayList.get(i).getLatitude(), arrayList.get(i).getLongtidude());
//        Log.d("devcpp", "Latitude " + imHere.getLatitude());
//        Log.d("devcpp", "Altitude " + imHere.getLongitude());

            MarkerOptions marker = new MarkerOptions().position(currentPosition).title(arrayList.get(i).getPlaceName());
//            marker.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_places));
            marker.icon(BitmapDescriptorFactory.fromBitmap(arrayList.get(i).getPlaceIcon()));
            mMap.addMarker(marker);
        }

    }
}
