package com.example.zador_000.restofinder.networkWrappers;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.util.Log;

import com.example.zador_000.restofinder.GeoPositionChecker;
import com.example.zador_000.restofinder.Places;
import com.example.zador_000.restofinder.SearchResultActivity;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by zador_000 on 01-Jun-17.
 */

public class PlaceNetworkWrapper {
    private Places places;
    private SearchResultActivity searchResultActivity;
    private Location imHere = GeoPositionChecker.imHere;
    private String location = "location=";
    private int radius;
    private String type = "";
    private String keyword = "keyword=суши";
    private static final int RESPONSE_CODE_OK = 200;
    private static final String KEY_AUTH = "key=AIzaSyA3WZdDlnhbupjcsLSxJa_0CSeDT8Trq4I";
//    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=-33.8670522,151.1957362&radius=500&type=restaurant&keyword=cruise&key=";

    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

    public PlaceNetworkWrapper(int radius, String type){
        this.radius = radius;
        this.type = type;
    }
    public ArrayList placesSearch() {
        ArrayList <Places> arrayList = new ArrayList<>();
        String strUrl = BASE_URL + location + imHere.getLatitude() + "," + imHere.getLongitude() +
                "&" + "radius=" + radius + "&type=" + type + "&" + KEY_AUTH;

        try {
            URL url = new URL(strUrl);
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(10000);
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("charset", "utf-8");

            connection.connect();

            int response = connection.getResponseCode();
            if (response == RESPONSE_CODE_OK){
                InputStream is = connection.getInputStream();
                String strResponse = "";
                if(is!=null){
                    BufferedReader reader = null;
                    reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));

                    if(reader!=null){
                        StringBuilder stringBuilder = new StringBuilder();
                        while (true){
                            String tempStr = reader.readLine();
                            if(tempStr!=null) {
                                stringBuilder.append(tempStr);
                            }else{
                                strResponse = stringBuilder.toString();
                                break;
                            }
                            }
                            reader.close();
                        }
                        is.close();
                    }
                Log.d("devanz", "response->" + response);
                Log.d("devanz", "strResponse->" + strResponse);
                Log.d("devanz", "strUrl->" + strUrl);
                Log.d("devanz", "distance->" + radius);

                JSONObject jsonObject = new JSONObject(strResponse);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                int arrayCount = jsonArray.length();
                for (int i= 0; i < arrayCount; i++){
                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                    double latitude = jsonObject1.getJSONObject("geometry").getJSONObject("location").getDouble("lat");
                    double longtidude = jsonObject1.getJSONObject("geometry").getJSONObject("location").getDouble("lng");
                    String name = jsonObject1.getString("name");
                    String iconURL = jsonObject1.getString("icon");
                    Bitmap placeIcon = getBitmapFromURL(iconURL);
                    places = new Places(latitude,longtidude,name,placeIcon);
                    arrayList.add(places);

                    Log.d("devanz", "latitude->" + latitude);
                    Log.d("devanz", "longtidude->" + longtidude);
                    Log.d("devanz", "longtidude->" + longtidude);
                    Log.d("devanz", "iconURL->" + iconURL);

                }
                }
            } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        } catch (ProtocolException e1) {
            e1.printStackTrace();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return arrayList;
    }


    public Bitmap getBitmapFromURL(String src) {
        try {
            java.net.URL url = new java.net.URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
